CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers

INTRODUCTION
------------
The Multilingual terms module was created to satisfy business requirements at one of the projects.
Module doesn't have any mandatory configurations.

  * For a full description of the module, visit the project page:
    link will be here soon

  * To submit bug reports and feature suggestions, or to track changes:
    link will be here soon

REQUIREMENTS
------------
This module requires the following modules:
 * Internationalization (https://drupal.org/project/i18n)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » Configuration » Regional and language » Multilingual taxonomy:

   - Show all options on site base language

     All terms (from all languages) will be displayed in option lists.
     This might be used if default language is used as "administration"
     language and other languages are displaying some actual content.

MAINTAINERS
-----------
Current maintainers:
 * Ilia Ivanov (Wunderkraut) - https://drupal.org/user/3099257
 * Nikita Izotov (Wunderkraut) - https://drupal.org/user/2990043


This project has been sponsored by:
 * Wunderkraut Estonia
   Visit our homepage: http://wunderkraut.ee/